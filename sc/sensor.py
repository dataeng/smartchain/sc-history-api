import logging
import requests
import time
import numpy as np

class Sensor:
  """
  Get sensor data using Fuelics API.
  
  :param sensor_id: Sensor ID.
  :type sensor_id: String
  
  :param api: API url.
  :type api: String
  
  :param key: API key.
  :type key: String
  """
  
  def __init__(self, sensor_id, api, key):
    self.api = api
    self.key = key
    self.sensor_id = sensor_id
    
    now = int(time.time())
        
    self.start_timestamp = now - (now % 86400)
    self.end_timestamp = now
  
  
  def setTimePeriod(self, start_timestamp, end_timestamp):
    """
    Sets the time period of interest.
    Getting measurements will refer to this specific periot.
    
    :param start_timestamp: Date to start (Unix time, UTC time zone)
    :type start_timestamp: Int
    
    :param end_timestamp: Date to end (Unix time, UTC time zone)
    :type end_timestamp: Int
    """
    
    self.start_timestamp = start_timestamp
    self.end_timestamp = end_timestamp
  
  
  def getMessages(self, message_type):
    """
    Performs an API call to Fuelics API to get sensor data within
    given time period
    
    :param message_type: Type of messages to get from the API.
    :type message_type: String
    """
    
    URL = self.api + "/" + self.sensor_id + "/messages/" + message_type
    HEADERS = { 
      'accept': 'application/json',
      'Authorization': 'Bearer ' + self.key
    }
    PARAMS = {
      'startTimestamp': self.start_timestamp,
      'endTimestamp': self.end_timestamp
    }
    r = requests.get(url=URL, params=PARAMS, headers=HEADERS)
    
    return r.json()['messages']
  
  
  def getLatest(self, message_type):
    """
    Performs an API call to Fuelics API to get latest sensor data.
    
    :param message_type: Type of messages to get from the API.
    :type message_type: String
    """
    
    URL = self.api + "/" + self.sensor_id + "/messages/" + message_type + "/latest"
    HEADERS = { 
      'accept': 'application/json',
      'Authorization': 'Bearer ' + self.key
    }
    r = requests.get(url=URL, headers=HEADERS)
    
    return r.json()['messages']
    
  
  def getMessagesFallback(self, message_type):
    """
    Calls API te get messages within the time range.
    If this is empty fall back to latest one
    
    :param message_type: Type of messages to get from the API.
    :type message_type: String
    """
    
    rtrn = self.getMessages(message_type)
    if len(rtrn) == 0:
      return self.getLatest(message_type)
    else:
      return rtrn
  
  
  def getMeasurements(self):
    """
    Get measurements (messages of type w)
    """
    logging.info("retrieving measurements from sensor")
    return self.getMessages("w")
  
  
  def getPositions(self):
    """
    Get positions (messages of type u)
    """
    logging.info("retrieving positions from sensor")
    return self.getMessagesFallback("u")
  
  
  def getMeasurementsPositions(self):
    """
    Get measurements with (linearly interpolated) positions
    """
    measurements = self.getMeasurements()
    positions = self.getPositions()
    results = []
    
    logging.info("iterpolating positions")
    
    times = list(map(lambda x: x['timestamp'], positions))
    lon   = list(map(lambda x: x['longitude'], positions))
    lat   = list(map(lambda x: x['latitude' ], positions))
    t     = list(map(lambda x: x['timestamp'], measurements))
    
    lonint = np.interp(t, times, lon)
    latint = np.interp(t, times, lat)
    
    logging.info("combining measurements and interpolated positions")
    
    i=0
    for m in measurements:
      n = {
        'timestamp':      m['timestamp'],
        'latitude':       latint[i],
        'longitude':      lonint[i],
        'temperature':    m['temperature'],
        'humidity':       m['relativeHumidity'],
        'inclination':    m['inclination']
      }
      results.append(n)
      i = i+1
    
    return results

