import logging
import requests
import time

class Weather:
  """
  Get weather data using OpenWeatherMap API.
    
  :param api: API url.
  :type api: String
  
  :param key: API key.
  :type key: String
  """
  
  def __init__(self, api, key):
    self.api = api
    self.key = key
  
  
  def getHistoricWeather(self, lat, lon, timestamp):
    """
    Performs an API call to the OpenWeatherMap API for historical
    weather data for a specific place and time in the past.
    
    :param lat: Geographical coordinate (latitude)
    :type lat: Float
    
    :param lon: Geographical coordinate (latitude)
    :type lon: Float
    
    :param timestamp: Date (Unix time, UTC time zone)
    :type timestamp: Int
    """
    
    URL = self.api
    PARAMS = {
      'lat': lat,
      'lon': lon,
      'dt': timestamp,
      'units': "metric",
      'appid': self.key
    }
    r = requests.get(url=URL, params=PARAMS)
    
    return r.json()
  
  
  def getWeather(self, lat, lon, timestamp):
    """
    Returns historical weather data for a specific place and time
    using OpenWeatherMap API. For free users of the API, weather data
    are available for 5 days in the past.
    
    :param lat: Geographical coordinate (latitude)
    :type lat: Float
    
    :param lon: Geographical coordinate (latitude)
    :type lon: Float
    
    :param timestamp: Date (Unix time, UTC time zone)
    :type timestamp: Int
    """
    
    weather = self.getHistoricWeather(lat, lon, timestamp)
    if ('current' in weather):
      return weather['current']
    
    logging.warning(weather)
    return None
  
  
  def extendWithWeather(self, messages):
    """
    Given a set of messages (i.e., dictionaries which contain 
    longitude, latitude and timestamp fields), extend them with
    weather data in a bind-join fashion.
    
    :param lat: messages
    :type lat: List[Dict]
    """
    
    logging.info("extending sensor data with open weather data")
    results = []
    
    for m in messages:
      lon = m['longitude']
      lat = m['latitude' ]
      t   = m['timestamp']
      
      weather = self.getWeather(lat, lon, t)
      
      if (weather != None):
        w = {
          'outside_temperature': weather['temp'],
          'outside_feels_like':  weather['feels_like'],
          'outside_humidity':    weather['humidity'],
          'outside_pressure':    weather['pressure'],
          'outside_clouds':      weather['clouds'],
          'outside_visibility':  weather['visibility'],
          'outside_wind_speed':  weather['wind_speed']
        }
        n = {**m, **w}
        results.append(n)
      
      else:
        results.append(m)
    
    return results

