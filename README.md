# SC-History API

This API used to augment historical sensor data (obtained from Fuelics API)
with historical weather data (obtained from OpenWeatherMap API)
at the time and place where the sensor measurement was made.

## How to configure

The first step is to configure SC-History. To do so, you have to edit
`app.ini` configuration file.

Regarding the sensor API configuration, go to the `[SENSOR]` section
of the API, and complete the `API` and `KEY` parameters with the URL
of the service and your API key respectively.
Alternatively, you can use the MockUp API here instead. Leave the
`KEY` parameter blank in this case.

Regarding the open weather API configuration, create a free
[account](https://home.openweathermap.org/users/sign_up)
in OpenWeatherMap to get an API key for using OpenWeatherMap API.
Place your API key in the `KEY` parameter of the `[WEATHER]` section
of the configuration file. 

## How to run

To build the Docker image for SC-History, do the following:

```
docker build -t sc_history .
```

To run SC-History on port 5000 do the following:

```
docker run -p 5000:5000 sc_history
```

If you don't want to use Docker, install Python3 in your system and
follow the steps shown in the Dockerfile.

## How to access

The API can be accessed by

```
http://localhost:5000/
```

As an example, the call

```
http://localhost:5000/SNV0XYZ/history?startTimestamp=1648339200&endTimestamp=1648340721
```

will return the sensor data with their corresponding weather data
from the sensor with sensor id `SNV0XYZ` in the time range from UNIX
timestamp `1648339200` to `1648340721`.

If you leave the timestamp parameters empty, i.e.,

```
http://localhost:5000/SNV0XYZ/history
```

the corresponding time range will begin 00:00 UTC of the current day
and will end the current time.


