from sc import *
import flask, configparser, json
from flask import request, jsonify
import logging

# initialize flask

app = flask.Flask(__name__)
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
app.config["JSON_SORT_KEYS"] = False

logging.root.setLevel(logging.NOTSET)

# initialize history API

config = configparser.ConfigParser()
config.read('app.ini')

fuelics_api = config['SENSOR']['API']
fuelics_key = config['SENSOR']['KEY']

openweathermap_api = config['WEATHER']['API']
openweathermap_key = config['WEATHER']['KEY']

# routes

@app.route('/', methods=['GET'])
def home():
  return "<h1>SC-History API</h1><p>This site is a prototype API which combines sensor data with open weather data.</p>"


@app.route('/<sensor_id>/history', methods=['GET'])
def get_history(sensor_id):
  
  s = Sensor(sensor_id, fuelics_api, fuelics_key)
  w = Weather(openweathermap_api, openweathermap_key)
  
  if ('startTimestamp' in request.args and 'endTimestamp' in request.args):
    start = int(request.args['startTimestamp'])
    end   = int(request.args['endTimestamp'])
    s.setTimePeriod(start, end)
  
  data = s.getMeasurementsPositions()
  data = w.extendWithWeather(data)
  
  return jsonify(data)

if __name__ == "__main__":
  app.run()

